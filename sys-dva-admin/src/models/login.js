/* eslint-disable no-undef */
import * as userServcer from '../services/user';
import { constant } from '../utils/config';

const base64 = require('base64-utf8');

export default {
  namespace: 'login',
  state: {
    loginLoading: false,
  },
  reducers: {
    showLoginLoading(state) {
      return {
        ...state,
        loginLoading: true,
      };
    },
    hideLoginLoading(state) {
      return {
        ...state,
        loginLoading: false,
      };
    },
  },
  effects: {
    *login({ payload }, { call, put }) {
      yield put({ type: 'showLoginLoading' });
      const data = yield call(userServcer.login, payload);
      yield put({ type: 'hideLoginLoading' });
      if (!data.err) {
        const token = data.data.token;
        yield window.localStorage.setItem(constant.tokenLocalStorageKey, token);
        let tokenDataArray = [];
        tokenDataArray = token.split('.');
        const userData = base64.decode(tokenDataArray[1]);
        yield window.localStorage.setItem(constant.userLocalStorageKey, userData);
        yield put({ type: 'app/enterAuth' });
      } else {
        throw Error('用户名或密码错误!');
      }
    },
  },
  subscriptions: {},
};
