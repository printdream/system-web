import modelExtend from 'dva-model-extend';
import { pageModel } from './common';
import * as userServer from '../services/user';
import * as roleService from '../services/role';

// 继承自pageModel
export default modelExtend(pageModel, {
  namespace: 'user',
  state: {
    selectedRowKeys: [],
    modalVisible: false,
    record: {},
    roleList: [],
  },
  reducers: {
    save(state, action) {
      return { ...state, ...action.payload };
    },
  },
  effects: {
    *query({ payload: { current = 1 } }, { call, put }) {
      const response = yield call(userServer.query, { current });
      if (!response.err) {
        const { data } = response;
        const list = yield data.records;
        const total = yield data.total;
        yield put({
          type: 'querySuccess',
          payload: {
            list,
            pagination: {
              current: Number(current),
              total: Number(total),
            },
          },
        });
      }
    },
    *create({ payload }, { call, put }) {
      const { err } = yield call(userServer.create, payload);
      if (!err) {
        yield put({ type: 'query', payload: { current: 1 } });
        yield put({ type: 'app/success', payload: { text: '操作成功' } });
      }
    },
    *update({ payload }, { call, put }) {
      let { birthDate } = yield payload;
      if (birthDate) {
        yield birthDate = birthDate.format('x');
      }
      const data = yield { ...payload, birthDate };
      const { err } = yield call(userServer.update, data);
      if (!err) {
        yield put({ type: 'query', payload: { current: 1 } });
        yield put({ type: 'app/success', payload: { text: '操作成功' } });
      }
    },
    *deleteOne({ payload }, { call, put }) {
      const { err } = yield call(userServer.deleteOne, payload);
      if (!err) {
        yield put({ type: 'query', payload: { current: 1 } });
        yield put({ type: 'app/success', payload: { text: '操作成功' } });
      }
    },
    *deleteList({ payload }, { call, put }) {
      const { err } = yield call(userServer.deleteList, payload);
      if (!err) {
        yield put({ type: 'query', payload: { current: 1 } });
        yield put({ type: 'app/success', payload: { text: '操作成功' } });
      }
    },
    *queryRole({payload}, {call, put}) {  // eslint-disable-line
      const { data } = yield call(roleService.query);
      yield put({ type: 'save', payload: { roleList: data } });
    },
  },
  subscriptions: {
    setup({ dispatch, history }) {
      history.listen((location) => {
        if (location.pathname === '/user') {
          dispatch({
            type: 'query',
            payload: location.query,
          });
          dispatch({
            type: 'queryRole',
          });
        }
      });
    },
  },
});
