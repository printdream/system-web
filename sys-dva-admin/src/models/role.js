import * as roleService from '../services/role';
import * as authServer from '../services/auth';
import { arrayToTree } from '../utils';

export default {

  namespace: 'role',

  state: {
    list: [],
    authList: [],
    selectAuth: [],
    currentRoleName: '',
    modalVisible: false,
    modalMethod: '',
  },

  subscriptions: {
    setup({ dispatch, history }) {  // eslint-disable-line
      history.listen((location) => {
        if (location.pathname === '/role') {
          dispatch({ type: 'putSelectAuth', payload: { selectAuth: [] } });
          dispatch({ type: 'save', payload: { currentRoleName: '' } });

          dispatch({ type: 'query' });
          dispatch({ type: 'queryAuth' });
        }
      });
    },
  },

  effects: {
    *query({payload}, {call, put}) {  // eslint-disable-line
      const { data } = yield call(roleService.query);
      yield put({ type: 'save', payload: { list: data } });
    },
    *queryAuth({payload}, {call, put}) {  // eslint-disable-line
      const { data } = yield call(authServer.query);
      const retData = yield arrayToTree(data, 'authName', 'p1');
      yield put({
        type: 'save',
        payload: {
          authList: retData,
        },
      });
    },
    *addRoleAuth({ payload }, { call, put }) {
      const { roleAuths } = payload;
      const { err } = yield call(roleService.addRoleAuth, roleAuths);
      if (!err) {
        yield put({ type: 'query' });
        yield put({ type: 'app/success', payload: { text: '操作成功' } });
      }
    },
    *addRole({ payload }, { call, put }) {
      const { values } = payload;
      const { err } = yield call(roleService.addRole, values);
      if (!err) {
        yield put({ type: 'query' });
        yield put({ type: 'app/success', payload: { text: '操作成功' } });
      }
    },
    *updateRole({ payload }, { call, put }) {
      const { values } = payload;
      const { err } = yield call(roleService.updateRole, values);
      if (!err) {
        yield put({ type: 'query' });
        yield put({ type: 'app/success', payload: { text: '操作成功' } });
      }
    },

    *deleteRole({ payload }, { call, put }) {
      const { values } = payload;
      const { err } = yield call(roleService.deleteRole, values);
      if (!err) {
        yield put({ type: 'query' });
        yield put({ type: 'app/success', payload: { text: '操作成功' } });
      }
    },
  },

  reducers: {
    save(state, action) {
      return { ...state, ...action.payload };
    },
    putSelectAuth(state, { payload }) {
      return { ...state, ...payload };
    },
    putCurrentRoleName(state, { payload }) {
      const { currentRoleName } = payload;
      return { ...state, currentRoleName };
    },
    showModal(state, { payload }) {
      return { ...state, ...payload, modalVisible: true };
    },
    hideModal(state) {
      return { ...state, modalVisible: false };
    },
  },

};
