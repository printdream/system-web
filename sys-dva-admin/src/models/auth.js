import * as authServer from '../services/auth';
import { arrayToTree } from '../utils';

export default {

  namespace: 'auth',

  state: {
    lsit: [],
    modalVisible: false,
    modalMethod: '',
    record: {},
  },

  subscriptions: {
    setup({ dispatch, history }) {  // eslint-disable-line
      history.listen((location) => {
        if (location.pathname === '/auth') {
          dispatch({ type: 'query' });
        }
      });
    },
  },

  effects: {
    *query({ payload }, { call, put }) {  // eslint-disable-line
      const { data } = yield call(authServer.query);
      const retData = yield arrayToTree(data, 'authName', 'p1');
      yield put({
        type: 'save',
        payload: {
          list: retData,
        },
      });
    },
    *create({ payload }, { call, put }) {
      const { err } = yield call(authServer.create, { payload });
      if (!err) {
        yield put({ type: 'query', payload: {} });
        yield put({ type: 'app/success', payload: { text: '操作成功' } });
      }
    },
    *deleteOne({ payload }, { call, put }) {
      const { err } = yield call(authServer.deleteOne, { payload });
      if (!err) {
        yield put({ type: 'query', payload: {} });
        yield put({ type: 'app/success', payload: { text: '操作成功' } });
      }
    },
    *update({ payload }, { call, put }) {
      const { err } = yield call(authServer.update, { payload });
      if (!err) {
        yield put({ type: 'query', payload: {} });
        yield put({ type: 'app/success', payload: { text: '操作成功' } });
      }
    },
  },

  reducers: {
    save(state, action) {
      return { ...state, ...action.payload };
    },
    showModal(state, { payload }) {
      return { ...state, ...payload, modalVisible: true };
    },
    hideModal(state) {
      return { ...state, modalVisible: false };
    },
  },

};
