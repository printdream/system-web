/* eslint-disable no-undef */
import { message } from 'antd';
import { routerRedux } from 'dva/router';
import { config } from '../utils';

const { prefix, constant } = config;

export default {
  namespace: 'app',
  state: {
    isLogin: false,
    user: {},
    // 状态研究
    menuPopoverVisible: false,
    siderFold: localStorage.getItem(`${prefix}siderFold`) === 'true',
    darkTheme: localStorage.getItem(`${prefix}darkTheme`) === 'true',
    isNavbar: document.body.clientWidth < 769,
    navOpenKeys: JSON.parse(localStorage.getItem(`${prefix}navOpenKeys`)) || [],
  },
  reducers: {
    hasToken(state) {
      return {
        ...state,
        isLogin: true,
      };
    },
    putUser(state, { payload }) {
      return {
        ...state,
        user: payload,
      };
    },
    // todo
    switchSider(state) {
      localStorage.setItem(`${prefix}siderFold`, !state.siderFold);
      return {
        ...state,
        siderFold: !state.siderFold,
      };
    },

    switchTheme(state) {
      localStorage.setItem(`${prefix}darkTheme`, !state.darkTheme);
      return {
        ...state,
        darkTheme: !state.darkTheme,
      };
    },

    switchMenuPopver(state) {
      return {
        ...state,
        menuPopoverVisible: !state.menuPopoverVisible,
      };
    },

    handleNavbar(state, { payload }) {
      return {
        ...state,
        isNavbar: payload,
      };
    },

    handleNavOpenKeys(state, { payload: navOpenKeys }) {
      return {
        ...state,
        ...navOpenKeys,
      };
    },
  },
  effects: {
    *success({ payload }) {
      const { text } = yield payload;
      yield message.success(text);
    },
    *enterAuth({ payload }, { put }) {
      // 验证LocalStorage中是否存在key:token是否存在
      // eslint-disable-next-line no-undef
      const token = yield window.localStorage.getItem(constant.tokenLocalStorageKey);
      if (token) {
        yield put({ type: 'hasToken' });
      } else {
        yield put({ type: 'authFail' });
      }
      if (token) {
        // 查询LocalStorage中用户信息
        // eslint-disable-next-line no-undef
        const user = yield window.localStorage.getItem(constant.userLocalStorageKey);
        if (user) {
          yield put({ type: 'putUser', payload: JSON.parse(user) });
        }
        if (location.pathname === '/' || location.pathname === '/login') {
          yield put(routerRedux.push('/user'));
        }
      } else {
        yield put(routerRedux.push('/login'));
      }
    },
    *changeNavbar({ payload }, { put, select }) {
      const { app } = yield (select(_ => _));
      const isNavbar = document.body.clientWidth < 769;
      if (isNavbar !== app.isNavbar) {
        yield put({ type: 'handleNavbar', payload: isNavbar });
      }
    },
    *logout() {
      yield window.localStorage.clear(constant.tokenLocalStorageKey);
      yield window.localStorage.clear(constant.userLocalStorageKey);
      yield window.location.replace('/login');
    },
  },
  subscriptions: {
    setup({ dispatch }) {
      dispatch({ type: 'enterAuth' });
      // todo
      let tid;
      window.onresize = () => {
        clearTimeout(tid);
        tid = setTimeout(() => {
          dispatch({ type: 'changeNavbar' });
        }, 300);
      };
    },
  },
};
