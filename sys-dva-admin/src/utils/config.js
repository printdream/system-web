module.exports = {
  name: 'SYS Admin',
  prefix: 'sysAdmin',
  footerText: 'Sys  © 2017 Dreamcatchers',
  logo: '/logo.png',
  iconFontCSS: '/iconfont.css',
  iconFontJS: '/iconfont.js',
  baseURL: 'http://localhost:8000/api/v1',
  openPages: ['/login'],
  apiPrefix: '/api/v1',
  api: {
    userLogin: '/user/login',
    userLogout: '/user/logout',
    userInfo: '/userInfo',
    users: '/users',
    posts: '/posts',
    user: '/user/:id',
    dashboard: '/dashboard',
  },
  constant: {
    tokenLocalStorageKey: 'sysAdminToken',
    userLocalStorageKey: 'sysAdminUser',

  },
};
