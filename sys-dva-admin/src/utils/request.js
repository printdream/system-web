/* eslint-disable no-undef,no-param-reassign */
import { constant } from '../utils/config';
import fetch from 'dva/fetch';

function parseJSON(response) {
  try {
    return response.json();
  } catch (err) {
    // const error = new Error(err.message);
    throw err;
  }
}

function checkStatus(response) {
  if (response.status >= 200 && response.status < 300) {
    return response;
  } else if (response.status === 401) {
    const error = new Error('无api权限或未登录！');
    throw error;
  } else {
    const error = new Error(response.statusText);
    error.response = response;
    throw error;
  }
}

/**
 * Requests a URL, returning a promise.
 *
 * @param  {string} url       The URL we want to request
 * @param  {object} [options] The options we want to pass to "fetch"
 * @return {object}           An object containing either "data" or "err"
 */
export default function request(url, options) {
  const token = localStorage.getItem(constant.tokenLocalStorageKey);
  if (token) {
    options.headers.authorization = token;
  }

  return fetch(url, options)
    .then(checkStatus)
    .then(parseJSON)
    .then(data => ({ data }))
    .catch(err => ({ err }));
}
