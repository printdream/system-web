/* eslint-disable no-underscore-dangle */
import React from 'react';
import { Router } from 'dva/router';

import App from './routes/app.js';
/**
 * 动态路由+webpack分片
 * http://robin-front.github.io/2016/04/18/react-router%E5%8A%A8%E6%80%81%E8%B7%AF%E7%94%B1%E4%B8%8EWebpack%E5%88%86%E7%89%87thunks/
 * @param app
 * @param model
 */
const registerModel = (app, model) => {
  if (!(app._models.filter(m => m.namespace === model.namespace).length === 1)) {
    app.model(model);
  }
};

function RouterConfig({ history, app }) {
  const route = {
    path: '/',
    component: App,
    getIndexRoute(nextState, cb) {
      require.ensure([], (require) => {
        registerModel(app, require('./models/user'));
        cb(null, { component: require('./routes/user/') });
      }, 'user');
    },
    childRoutes: [
      {
        path: 'login',
        getComponent(nextState, cb) {
          require.ensure([], (require) => {
            registerModel(app, require('./models/login'));
            cb(null, require('./routes/login/login'));
          }, 'login');
        },
      },
      {
        path: 'user',
        getComponent(nextState, cb) {
          require.ensure([], (require) => {
            registerModel(app, require('./models/user'));
            cb(null, require('./routes/user/'));
          }, 'user');
        },
      },
      {
        path: 'role',
        getComponent(nextState, cb) {
          require.ensure([], (require) => {
            registerModel(app, require('./models/role'));
            cb(null, require('./routes/role/'));
          }, 'role');
        },
      },
      {
        path: 'auth',
        getComponent(nextState, cb) {
          require.ensure([], (require) => {
            registerModel(app, require('./models/auth'));
            cb(null, require('./routes/auth/'));
          }, 'auth');
        },
      },

    ],
  };
  return (<Router history={history} routes={route} />
  );
}

export default RouterConfig;
