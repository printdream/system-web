import './index.html'
import 'babel-polyfill'
import dva from 'dva';
import createLoading from 'dva-loading';
import { browserHistory } from 'dva/router';
import { message } from 'antd';


// 1. Initialize
const app = dva({
  ...createLoading({
    effects: true,
  }),
  history: browserHistory,
  onError(error) {
    message.error(`错误: ${error.message}`, 4);
  },
});

// 2. Plugins
// app.use({});

// 3. Model
app.model(require('./models/app'));

// app.model(require('./models/user'));

// 4. Router
app.router(require('./router'));

// 5. Start
app.start('#root');
