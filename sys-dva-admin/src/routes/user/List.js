/**
 * Created by deantsui on 2017/6/26.
 */
import React from 'react';
import { Table, Pagination, Modal } from 'antd';
import { routerRedux } from 'dva/router';
import styles from './List.less';
import DropOption from '../../components/DropOption/DropOption';

const confirm = Modal.confirm;

const List = ({ dispatch, loading, list, total, current, selectedRowKeys }) => {
  function pageChangeHandler(page) {
    dispatch(routerRedux.push({
      pathname: '/user',
      query: { current: page },
    }));
  }
  const handleMenuClick = (record, { key }) => {
    // 1:更新 2:删除
    if (key === '1') {
      dispatch({ type: 'user/showModal', payload: { record, modalMethod: 'update' } });
    } else if (key === '2') {
      confirm({
        title: '你确定要删除此条记录？',
        onOk() {
          dispatch({ type: 'user/deleteOne', payload: { ...record } });
        },
      });
    }
  };
  const rowSelection = {
    selectedRowKeys,
    onChange: (_selectedRowKeys) => {
      dispatch({
        type: 'user/updateState',
        payload: { selectedRowKeys: _selectedRowKeys } });
    },
  };

  const columns = [
    { title: '用户ID',
      dataIndex: 'userID',
      key: 'userID' },
    { title: '用户名',
      dataIndex: 'userName',
      key: 'userName',
      render: text => <a href="">{text}</a>,
    },
    { title: '登录账号',
      dataIndex: 'loginName',
      key: 'loginName',
    },
    {
      title: '角色',
      key: 'roles',
      render: (text, record) => {
        const roleName = record.roles[0].roleName;
        return (roleName || '-');
      },
    },
    { title: '性别',
      dataIndex: 'sex',
      key: 'sex',
      render: (text) => { return text || '-'; },
    },
    { title: '生日',
      dataIndex: 'birthDate',
      key: 'birthDate',
      render: (text) => {
        const ret = text ? (new Date(text)).toLocaleDateString() : '-';
        return ret;
      },
    },
    { title: 'Email',
      dataIndex: 'email',
      key: 'email',
      render: (text) => { return text || '-'; },
    },
    { title: '手机',
      dataIndex: 'mobilPhone',
      key: 'mobilPhone',
      render: (text) => { return text || '-'; },
    },
    { title: '更新时间',
      dataIndex: 'updateDate',
      key: 'updateDate',
      render: (text) => { return (new Date(text)).toLocaleString() || '-'; },
    },
    {
      title: '操作',
      key: 'operation',
      width: 100,
      render: (text, record) => {
        return (<DropOption
          onMenuClick={e => handleMenuClick(record, e)}
          menuOptions={
            [{ key: '1', name: '更新' }, { key: '2', name: '删除' }]
          }
        />);
      },
    },
  ];

  return (
    <div>
      <Table
        columns={columns}
        dataSource={list}
        rowKey={record => record.loginName}
        pagination={false}
        className={styles.table}
        loading={loading}
        rowSelection={rowSelection}
      />

      <Pagination
        className="ant-table-pagination"
        total={total}
        current={current}
        defaultCurrent={1}
        pageSize={5}
        onChange={pageChangeHandler}
        showTotal={(_total, _range) => `${_range[0]}-${_range[1]} 页,共 ${_total} 条`}
      />
    </div>
  );
};
export default List;
