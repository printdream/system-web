/**
 * Created by deantsui on 2017/6/26.
 */
import React from 'react';
import { DatePicker, Form, Input, Modal, Radio, Select } from 'antd';
import moment from 'moment';

const FormItem = Form.Item;
const RadioGroup = Radio.Group;
const Option = Select.Option;

const userModal = ({ dispatch,
                     record,
                     modalVisible,
                     modalMethod,
                     onAdd,
                     onUpdate,
                     roleList,
                     form:
                       { getFieldDecorator,
                         validateFields,
                         getFieldValue,
                       },
}) => {
  const formItemLayout = {
    labelCol: { span: 6 },
    wrapperCol: { span: 14 },
  };

  const {
    loginName = null,
    loginPassword = null,
    userName = null,
    sex = null,
    email = null,
    mobilPhone = null,
    birthDate = null,
  } = record;

  let title;
  let onOk;

  const hideModelHandler = () => {
    dispatch({ type: 'user/hideModal' });
  };
  const okHandler = () => {
    validateFields((err, _values) => {
      if (!err) {
        onOk(_values);
        hideModelHandler();
      }
    });
  };
  const checkPassword = (rule, value, callback) => {
    if (value && value !== getFieldValue('loginPassword')) {
      callback('两次输入的密码不一致！');
    } else {
      callback();
    }
  };
  const handleChange = (value) => {
    console.log(`selected ${value}`);
  };
  const roleOptions = roleList.map((item, key) => {
    return <Option value={item.roleName}>{item.roleName}</Option>;
  });
  let formItems;

  if (modalMethod === 'add') {
    onOk = onAdd;
    title = '添加';
    formItems =
      (<div>
        <FormItem
          {...formItemLayout}
          label="登录账号"
        >
          {
            getFieldDecorator('loginName', {
              initialValue: loginName,
              rules: [
                {
                  required: true,
                  message: '必填项',
                }],
            })(<Input />)
          }
        </FormItem>
        <FormItem
          {...formItemLayout}
          label="密码"
        >
          {
            getFieldDecorator('loginPassword', {
              initialValue: loginPassword,
              rules: [
                {
                  required: !loginName,
                  message: '必填项',
                }],
            })(<Input type="password" />)
          }
        </FormItem>
        <FormItem
          {...formItemLayout}
          label="重复密码"
        >
          {
            getFieldDecorator('confirm', {
              initialValue: loginPassword,
              rules: [
                {
                  required: !loginName,
                  message: '必填项',
                }, {
                  validator: checkPassword,
                }],
            })(<Input type="password" />)
          }
        </FormItem>
        <FormItem
          {...formItemLayout}
          label="用户名"
        >
          {
            getFieldDecorator('userName', {
              initialValue: userName,
              rules: [
                {
                  required: true,
                  message: '必填项',
                }],
            })(<Input />)
          }
        </FormItem>
        <FormItem
          {...formItemLayout}
          label="角色"
        >
          {
            getFieldDecorator('roleName', {
              rules: [
                {
                  required: true,
                  message: '必填项',
                }],
            })(<Select style={{ width: 120 }} onChange={handleChange}>
              {roleOptions}
            </Select>)
          }
        </FormItem>
        <FormItem
          {...formItemLayout}
          label="性别"
        >
          {
            getFieldDecorator('sex', {
              initialValue: sex,
              rules: [
                {
                  required: true,
                  message: '必填项',
                }],
            })(<RadioGroup>
              <Radio value="男">男</Radio>
              <Radio value="女">女</Radio>
            </RadioGroup>)
          }
        </FormItem>
        <FormItem
          {...formItemLayout}
          label="Email"
        >
          {
            getFieldDecorator('email', {
              initialValue: email,
              rules: [
                {
                  type: 'email',
                }],
            })(<Input />)
          }
        </FormItem>
        <FormItem
          {...formItemLayout}
          label="手机"
        >
          {
            getFieldDecorator('mobilPhone', {
              initialValue: mobilPhone,
              rules: [
                {
                  len: 11,
                }],
            })(<Input />)
          }
        </FormItem>
        <FormItem
          {...formItemLayout}
          label="出生日期"
        >
          {getFieldDecorator('birthDate', {
            initialValue: birthDate && moment(birthDate, 'x'),
            type: 'object',
          })(<DatePicker />)}
        </FormItem>
      </div>);
  } else if (modalMethod === 'update') {
    title = '更新';
    onOk = onUpdate;
    formItems =
      (<div>
        <FormItem
          {...formItemLayout}
          label="登录账号"
        >
          {
            getFieldDecorator('loginName', {
              initialValue: loginName,
              rules: [
                {
                  required: true,
                  message: '必填项',
                }],
            })(<Input disabled />)
          }
        </FormItem>
        <FormItem
          {...formItemLayout}
          label="密码"
        >
          {
            getFieldDecorator('loginPassword', {
              initialValue: loginPassword,
              rules: [
                {
                  required: !loginName,
                  message: '必填项',
                }],
            })(<Input type="password" />)
          }
        </FormItem>
        <FormItem
          {...formItemLayout}
          label="重复密码"
        >
          {
            getFieldDecorator('confirm', {
              initialValue: loginPassword,
              rules: [
                {
                  required: !loginName,
                  message: '必填项',
                }, {
                  validator: checkPassword,
                }],
            })(<Input type="password" />)
          }
        </FormItem>
        <FormItem
          {...formItemLayout}
          label="用户名"
        >
          {
            getFieldDecorator('userName', {
              initialValue: userName,
              rules: [
                {
                  required: true,
                  message: '必填项',
                }],
            })(<Input />)
          }
        </FormItem>
        <FormItem
          {...formItemLayout}
          label="角色"
        >
          {
            getFieldDecorator('roleName', {
              initialValue: record.roles[0].roleName,
              rules: [
                {
                  required: true,
                  message: '必填项',
                }],
            })(<Select style={{ width: 120 }} onChange={handleChange}>
              {roleOptions}
            </Select>)
          }
        </FormItem>
        <FormItem
          {...formItemLayout}
          label="性别"
        >
          {
            getFieldDecorator('sex', {
              initialValue: sex,
              rules: [
                {
                  required: true,
                  message: '必填项',
                }],
            })(<RadioGroup>
              <Radio value="男">男</Radio>
              <Radio value="女">女</Radio>
            </RadioGroup>)
          }
        </FormItem>
        <FormItem
          {...formItemLayout}
          label="Email"
        >
          {
            getFieldDecorator('email', {
              initialValue: email,
              rules: [
                {
                  type: 'email',
                }],
            })(<Input />)
          }
        </FormItem>
        <FormItem
          {...formItemLayout}
          label="手机"
        >
          {
            getFieldDecorator('mobilPhone', {
              initialValue: mobilPhone,
              rules: [
                {
                  len: 11,
                }],
            })(<Input />)
          }
        </FormItem>
        <FormItem
          {...formItemLayout}
          label="出生日期"
        >
          {getFieldDecorator('birthDate', {
            initialValue: birthDate && moment(birthDate, 'x'),
            type: 'object',
          })(<DatePicker />)}
        </FormItem>
      </div>);
  }
  return (
    <Modal
      title={title}
      onOk={okHandler}
      onCancel={hideModelHandler}
      visible={modalVisible}
    >
      <Form horizontal hasFeedback onSubmit={okHandler}>
        {formItems}
      </Form>
    </Modal>
  );
};
export default Form.create()(userModal);
