import React from 'react';
import { connect } from 'dva';
import { Button, Col, Popconfirm, Row } from 'antd';
import List from './List';
import UserModal from './UserModal';

const index = ({ location, dispatch, user, loading }) => {
  const {
    list,
    pagination: { current, total },
    selectedRowKeys, modalVisible,
    record,
    modalMethod,
    roleList} = user;

  const listProps = {
    list,
    total,
    current,
    loading: loading.effects['user/query'],
    location,
    dispatch,
    selectedRowKeys,
  };
  const modalProps = {
    dispatch,
    modalVisible,
    modalMethod,
    record,
    roleList,
    onAdd: (values) => {
      dispatch({
        type: 'user/create',
        payload: values,
      });
    },
    onUpdate: (values) => {
      dispatch({
        type: 'user/update',
        payload: values,
      });
    },
  };
  const ColProps = {
    xs: 24,
    sm: 12,
    style: {
      marginBottom: 16,
    },
  };

  const TwoColProps = {
    ...ColProps,
    xl: 96,
  };
  const handleDeleteItems = () => {
    const LIST = [];
    selectedRowKeys.forEach((item) => { LIST.push({ loginName: item }); });
    dispatch({ type: 'user/deleteList', payload: LIST });
  };

  const handleAddClick = () => {
    dispatch({ type: 'user/showModal', payload: { record: {}, modalMethod: 'add' } });
  };
  return (
    <div className="content-inner">
      <Row {...TwoColProps} xl={{ span: 10 }} md={{ span: 24 }} sm={{ span: 24 }}>
        <div style={{ display: 'flex', justifyContent: 'space-between' }}>
          <div>
            <Button size="large" type="ghost" onClick={handleAddClick}>添加</Button>
          </div>
        </div>
      </Row>
      {
        selectedRowKeys.length > 0 &&
        <Row style={{ marginBottom: 24, textAlign: 'right', fontSize: 13 }}>
          <Col>
            {`${selectedRowKeys.length} 条记录`}
            <Popconfirm title={'你确定要删除选中数据吗?'} placement="left" onConfirm={handleDeleteItems}>
              <Button type="primary" size="large" style={{ marginLeft: 8 }}>删除</Button>
            </Popconfirm>
          </Col>
        </Row>
      }
      <List {...listProps} />
      {modalVisible && <UserModal {...modalProps} />}
    </div>
  );
};


export default connect(({ user, loading }) => ({ user, loading }))(index);
