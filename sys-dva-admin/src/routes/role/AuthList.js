/**
 * Created by deantsui on 2017/7/3.
 */
import React from 'react';
import { Table, Icon } from 'antd';


const AuthList = ({ list, loading, dispatch, selectAuth }) => {
  const columns = [{
    title: '权限名',
    key: 'authNote',
    dataIndex: 'authNote',
    width: '30%',
    render: (text, record) => {
      return (
        <span>
          <Icon type={record.p2} style={{ fontSize: 16, marginRight: 4 }} />
          {text}
        </span>
      );
    },
  }, {
    title: '编码',
    key: 'authName',
    dataIndex: 'authName',
    width: '60%',
  }];
  // rowSelection objects indicates the need for row selection
  const rowSelection = {
    selectedRowKeys: selectAuth,
    onChange: (selectedRowKeys) => {
      dispatch({ type: 'role/putSelectAuth', payload: { selectAuth: selectedRowKeys } });
    },
  };
  const getExpandedRowKeys = list.map((item) => {
    if (item.p1 === null || item.p1 === '') {
      return item.authName;
    }
  });
  return (
    <Table
      rowKey={record => record.authName}
      indentSize={50}
      columns={columns}
      dataSource={list}
      pagination={false}
      loading={loading}
      rowSelection={rowSelection}
      expandedRowKeys={getExpandedRowKeys}
    />
  );
};
export default AuthList;
