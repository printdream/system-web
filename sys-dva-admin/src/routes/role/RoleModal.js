/**
 * Created by deantsui on 2017/6/26.
 */
import React from 'react';
import { Form, Input, Modal } from 'antd';

const FormItem = Form.Item;


const RoleModal = ({ dispatch,
                     modalVisible,
                     onAdd,
                     modalMethod,
                     form:
                       { getFieldDecorator,
                         validateFields,
                         getFieldValue,
                       },
}) => {
  const formItemLayout = {
    labelCol: { span: 6 },
    wrapperCol: { span: 14 },
  };


  let title;
  let onOk;
  const hideModelHandler = () => {
    dispatch({ type: 'role/hideModal' });
  };
  const okHandler = () => {
    validateFields((err, _values) => {
      if (!err) {
        onOk(_values);
        hideModelHandler();
      }
    });
  };
  let formItems;

  if (modalMethod === 'add') {
    onOk = onAdd;
    title = '添加';
    formItems =
      (<div>
        <FormItem
          {...formItemLayout}
          label="角色名"
        >
          {
          getFieldDecorator('roleName', {
            rules: [
              {
                required: true,
                message: '必填项',
              }],
          })(<Input />)
        }
        </FormItem>

      </div>);
  }


  return (
    <Modal
      title={title}
      onOk={okHandler}
      onCancel={hideModelHandler}
      visible={modalVisible}
    >
      <Form horizontal hasFeedback onSubmit={okHandler} >
        {formItems}
      </Form>
    </Modal>
  );
};
export default Form.create()(RoleModal);
