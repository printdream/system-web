import React from 'react';
import { connect } from 'dva';
import { Button, Col, Icon, Modal, Menu, Row } from 'antd';
import RoleModal from './RoleModal';
import styles from './index.less';
import AuthList from './AuthList';
import DropOption from '../../components/DropOption/DropOption';

const confirm = Modal.confirm;

const index = ({ location, dispatch, role, loading }) => {
  const { list, authList, selectAuth, currentRoleName, modalVisible, modalMethod } = role;
  const colProps = {
    xs: 8,
    md: 8,
    lg: 5,
    // xl: 3,
  };
  const colRightProps = {
    xs: 16,
    md: 16,
    lg: 19,
    // xl: 21,
  };
  const menuOnclick = ({ item, key }) => {
    if (key === 'add') {
      dispatch({ type: 'role/showModal', payload: { modalMethod: 'add' } });
    } else {
      const auths = [];
      // 构建权限名数组
      list.forEach((_role) => {
        if (_role.roleName === key) {
          _role.auths.forEach((auth) => {
            auths.push(auth.authName);
          });
        }
      });

      dispatch({ type: 'role/putSelectAuth', payload: { selectAuth: auths } });
      dispatch({ type: 'role/putCurrentRoleName', payload: { currentRoleName: key } });
    }
  };
  const menuItems = list.map((item) => {
    return <Menu.Item key={item.roleName}><Icon type="user" />{item.roleName}</Menu.Item>;
  });

  const modalProps = {
    dispatch,
    modalVisible,
    modalMethod,
    record: { roleName: currentRoleName },
    onAdd: (values) => {
      dispatch({ type: 'role/addRole', payload: { values } });
    },
  };
  const menuOptions = [{ key: '2', name: '删除' }];


  const handlePushClick = () => {
    const roleAuths = [];
    selectAuth.forEach((item) => {
      if (item !== null) {
        roleAuths.push({ authName: item, roleName: currentRoleName });
      }
    });
    // 数组为0添加权限名 清除权限角色数据
    if (roleAuths.length === 0) {
      roleAuths.push({ authName: '', roleName: currentRoleName });
    }
    dispatch({ type: 'role/addRoleAuth', payload: { roleAuths } });
  };

  const handleMenuClick = ({ key }) => {
    // 1:更新 2:删除
    if (key === '2') {
      confirm({
        title: '此操作会永久删除用户权限关系、权限角色关系，你确定要删除此条记录？',
        onOk() {
          dispatch({ type: 'role/deleteRole', payload: { values: { roleName: currentRoleName } } });
        },
      });
    }
  };

  return (
    <div>
      <Row gutter={16}>
        <Col {...colProps}>
          <div className="content-inner" >
            <Menu
              className={styles.noBorder}
              defaultSelectedKeys={['1']}
              defaultOpenKeys={['sub1']}
              mode="inline"
              onClick={menuOnclick}
            >
              <span>角色列表</span>
              <hr />
              {menuItems}
              <Menu.Item key="add"><Icon type="plus-circle-o" />添加角色</Menu.Item>
            </Menu>
          </div>
        </Col>
        <Col {...colRightProps}>
          <div className="content-inner" >
            <Row xl={{ span: 10 }} md={{ span: 24 }} sm={{ span: 24 }}>
              <div style={{ display: 'flex', justifyContent: 'space-between' }}>
                <span>{currentRoleName}</span>
                <div>
                  <Button size="large" type="ghost" onClick={handlePushClick}><Icon type="to-top" />提交</Button>
                  <DropOption
                    onMenuClick={e => handleMenuClick(e)}
                    menuOptions={menuOptions}
                  >更多</DropOption>
                </div>
              </div>
            </Row>
            {currentRoleName ?
              <AuthList list={authList} selectAuth={selectAuth} loading={loading.effects['role/queryAuth']} dispatch={dispatch} />
              : '请选择角色'
            }
          </div>
        </Col>
      </Row>
      {modalVisible && <RoleModal {...modalProps} />}
    </div>
  );
};

export default connect(({ role, loading }) => ({ role, loading }))(index);
