import React from 'react';
import { Button, Row } from 'antd';
import { connect } from 'dva';
import AuthModal from './AuthModal';
import List from './List';


const index = ({ location, dispatch, auth, loading }) => {
  const { list, modalVisible, record, modalMethod } = auth;
  const ColProps = {
    xs: 24,
    sm: 12,
    style: {
      marginBottom: 16,
    },
  };

  const TwoColProps = {
    ...ColProps,
    xl: 96,
  };

  const modalProps = {
    dispatch,
    modalVisible,
    record,
    modalMethod,
    onAdd: (values) => {
      dispatch({ type: 'auth/create', payload: { values } });
    },
    onUpdate: (values) => {
      dispatch({ type: 'auth/update', payload: { values } });
    },
    onAddParent: (values) => {
      dispatch({ type: 'auth/create', payload: { values } });
    },
  };

  const handleAddClick = () => {
    dispatch({ type: 'auth/showModal', payload: { record: {}, modalMethod: 'add' } });
  };

  return (
    <div className="content-inner">
      <Row {...TwoColProps} xl={{ span: 10 }} md={{ span: 24 }} sm={{ span: 24 }}>
        <div style={{ display: 'flex', justifyContent: 'space-between' }}>
          <div>
            <Button size="large" type="ghost" onClick={handleAddClick} >
              添加
            </Button>
          </div>
        </div>
      </Row>
      <List list={list} loading={loading.effects['auth/query']} dispatch={dispatch} />
      {modalVisible && <AuthModal {...modalProps} />}
    </div>
  );
};

export default connect(({ auth, loading }) => ({ auth, loading }))(index);
