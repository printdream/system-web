/**
 * Created by deantsui on 2017/7/3.
 */
import React from 'react';
import { Table, Modal, Icon } from 'antd';
import DropOption from '../../components/DropOption/DropOption';

const confirm = Modal.confirm;

const List = ({ list, loading, dispatch }) => {
  const handleMenuClick = (record, { key }) => {
    // 1:更新 2:删除
    if (key === '1') {
      dispatch({ type: 'auth/showModal', payload: { record, modalMethod: 'update' } });
    } else if (key === '2') {
      confirm({
        title: '你确定要删除此条记录？',
        onOk() {
          dispatch({ type: 'auth/deleteOne', payload: { values: record } });
        },
      });
    } else if (key === '3') {
      dispatch({ type: 'auth/showModal', payload: { record, modalMethod: 'addParent' } });
    }
  };
  const columns = [{
    title: '权限名',
    key: 'authNote',
    dataIndex: 'authNote',
    width: '30%',
    render: (text, record) => {
      return (
        <span>
          <Icon type={record.p2} style={{ fontSize: 16, marginRight: 4 }} />
          {text}
        </span>
      );
    },
  }, {
    title: '编码',
    key: 'authName',
    dataIndex: 'authName',
    width: '60%',
  }, {
    title: '操作',
    key: 'operation',
    render: (text, record) => {
      const menuOptions = [{ key: '3', name: '添加请求方式权限' }, { key: '1', name: '更新' }, { key: '2', name: '删除' }];
      if (!record.children) {
        menuOptions.shift();
      }
      return (<DropOption
        onMenuClick={e => handleMenuClick(record, e)}
        menuOptions={menuOptions}
      />);
    },
  }];

  return (
    <Table
      rowKey={record => record.authID}
      indentSize={50}
      columns={columns}
      dataSource={list}
      pagination={false}
      loading={loading}
    />
  );
};
export default List;
