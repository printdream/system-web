/**
 * Created by deantsui on 2017/6/26.
 */
import React from 'react';
import { Form, Input, Modal, Radio } from 'antd';

const FormItem = Form.Item;


const AuthModal = ({ dispatch,
                     record,
                     modalVisible,
                     onAdd,
                     onUpdate,
                     onAddParent,
                     modalMethod,
                     form:
                       { getFieldDecorator,
                         validateFields,
                         getFieldValue,
                       },
}) => {
  const formItemLayout = {
    labelCol: { span: 6 },
    wrapperCol: { span: 14 },
  };

  const {
    authName = null,
    authNote,
    url,
    p1,
    p2,
  } = record;
  let title;
  let onOk;
  const hideModelHandler = () => {
    dispatch({ type: 'auth/hideModal' });
  };
  const okHandler = () => {
    validateFields((err, _values) => {
      if (!err) {
        onOk(_values);
        hideModelHandler();
      }
    });
  };
  let formItems;

  if (modalMethod === 'add') {
    onOk = onAdd;
    title = '添加';
    formItems =
      (<div>
        <FormItem
          {...formItemLayout}
          label="权限编码"
        >
          {
          getFieldDecorator('authName', {
            rules: [
              {
                required: true,
                message: '必填项',
              }],
          })(<Input placeholder="例如：ROLE_TEST 警告：必须以ROLE_开头" />)
        }
        </FormItem>
        <FormItem
          {...formItemLayout}
          label="权限名"
        >
          {
          getFieldDecorator('authNote', {
            rules: [
              {
                required: true,
                message: '必填项',
              }],
          })(<Input placeholder="系统管理权限测试" />)
        }
        </FormItem>
        <FormItem
          {...formItemLayout}
          label="权限拦截资源"
        >
          {
          getFieldDecorator('url', {
            initialValue: url,
            rules: [
              {
                required: true,
                message: '必填项',
              }],
          })(<Input placeholder="例如: /system/user/**" />)
        }
        </FormItem>
        <FormItem
          {...formItemLayout}
          label="图标"
        >
          {
            getFieldDecorator('p2', {
              initialValue: ('key'),
              rules: [
                {
                }],
            })(<Input />)
          }
        </FormItem>
      </div>);
  } else if (modalMethod === 'addParent') {
    onOk = onAddParent;
    title = '添加请求方式权限';
    formItems =
      (<div>
        <FormItem
          {...formItemLayout}
          label="权限编码"
        >
          {
            getFieldDecorator('authName', {
              rules: [
                {
                  required: true,
                  message: '必填项',
                }],
            })(<div><Input addonBefore={`${authName}.`} placeholder="例：GET 或 POST" /></div>)
          }
        </FormItem>
        <FormItem
          {...formItemLayout}
          label="权限名"
        >
          {
            getFieldDecorator('authNote', {
              rules: [
                {
                  required: true,
                  message: '必填项',
                }],
            })(<Input addonBefore={`${authNote}-`} placeholder="例如：查询 或 添加" />)
          }
        </FormItem>
        <FormItem
          {...formItemLayout}
          label="权限拦截资源"
        >
          {
            getFieldDecorator('url', {
              initialValue: url,
              rules: [
                {
                  required: true,
                  message: '必填项',
                }],
            })(<Input placeholder="例如: /system/user/**" />)
          }
        </FormItem>
        <FormItem
          {...formItemLayout}
          label="父节点"
        >
          {
            getFieldDecorator('p1', {
              initialValue: authName,
              rules: [
                {
                  required: true,
                  message: '必填项',
                }],
            })(<Input disabled />)
          }
        </FormItem>
        <FormItem
          {...formItemLayout}
          label="图标"
        >
          {
            getFieldDecorator('p2', {
              initialValue: p2,
              rules: [
                {
                }],
            })(<Input />)
          }
        </FormItem>
      </div>);
  } else if (modalMethod === 'update') {
    title = '更新';
    onOk = onUpdate;
    formItems =
      (<div>
        <FormItem
          {...formItemLayout}
          label="权限编码"
        >
          {
            getFieldDecorator('authName', {
              initialValue: authName,
              rules: [
                {
                  required: true,
                  message: '必填项',
                }],
            })(<Input disabled placeholder="例如：ROLE_TEST 警告：必须以ROLE_开头" />)
          }
        </FormItem>
        <FormItem
          {...formItemLayout}
          label="权限名"
        >
          {
            getFieldDecorator('authNote', {
              initialValue: authNote,
              rules: [
                {
                  required: true,
                  message: '必填项',
                }],
            })(<Input placeholder="系统管理权限测试" />)
          }
        </FormItem>
        <FormItem
          {...formItemLayout}
          label="权限拦截资源"
        >
          {
            getFieldDecorator('url', {
              initialValue: url,
              rules: [
                {
                  required: true,
                  message: '必填项',
                }],
            })(<Input disabled placeholder="例如: /system/user/**" />)
          }
        </FormItem>
        <FormItem
          {...formItemLayout}
          label="图标"
        >
          {
            getFieldDecorator('p2', {
              initialValue: p2,
              rules: [
                {
                }],
            })(<Input />)
          }
        </FormItem>

      </div>);
  }


  return (
    <Modal
      title={title}
      onOk={okHandler}
      onCancel={hideModelHandler}
      visible={modalVisible}
    >
      <Form horizontal hasFeedback onSubmit={okHandler} >
        {formItems}
      </Form>
    </Modal>
  );
};
export default Form.create()(AuthModal);
