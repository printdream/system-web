/**
 * Created by deantsui on 2017/7/3.
 */
import request from '../utils/request';

export async function query() {
  return request('/api/system/auth/', {
    method: 'GET',
    headers: {
      'Content-Type': 'application/json',
    },
    mode: 'cors',
  });
}

export async function create({ payload }) {
  const { values } = payload;
  return request('/api/system/auth/', {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json',
    },
    mode: 'cors',
    body: JSON.stringify(values),
  });
}

export async function deleteOne({ payload }) {
  const { values } = payload;
  return request('/api/system/auth/', {
    method: 'DELETE',
    headers: {
      'Content-Type': 'application/json',
    },
    mode: 'cors',
    body: JSON.stringify(values),
  });
}

export async function update({ payload }) {
  const { values } = payload;
  return request('/api/system/auth/', {
    method: 'PUT',
    headers: {
      'Content-Type': 'application/json',
    },
    mode: 'cors',
    body: JSON.stringify(values),
  });
}
