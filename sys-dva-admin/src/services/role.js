/**
 * Created by deantsui on 2017/7/3.
 */
import request from '../utils/request';

export async function query() {
  return request('/api/system/role/', {
    method: 'GET',
    headers: {
      'Content-Type': 'application/json',
    },
    mode: 'cors',
  });
}

export async function addRoleAuth(payload) {
  return request('/api/system/roleAuth/', {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json',
    },
    mode: 'cors',
    body: JSON.stringify(payload),
  });
}

export async function addRole(payload) {
  return request('/api/system/role/', {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json',
    },
    mode: 'cors',
    body: JSON.stringify(payload),
  });
}

export async function deleteRole(payload) {
  return request('/api/system/role/', {
    method: 'DELETE',
    headers: {
      'Content-Type': 'application/json',
    },
    mode: 'cors',
    body: JSON.stringify(payload),
  });
}
export async function updateRole(payload) {
  return request('/api/system/role/', {
    method: 'PUT',
    headers: {
      'Content-Type': 'application/json',
    },
    mode: 'cors',
    body: JSON.stringify(payload),
  });
}
