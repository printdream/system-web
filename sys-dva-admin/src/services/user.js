import request from '../utils/request';

export async function login(payload) {
  return request('/api/system/auth/createToken', {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json',
    },
    mode: 'cors',
    body: JSON.stringify(payload),
  });
}

export async function query(payload) {
  const { current } = payload;
  return request(`/api/system/users?current=${current}&size=${5}`, {
    method: 'GET',
    headers: {
      'Content-Type': 'application/json',
    },
    mode: 'cors',
  });
}

export async function create(payload) {
  return request('/api/system/users/', {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json',
    },
    mode: 'cors',
    body: JSON.stringify(payload),
  });
}

export async function update(payload) {
  const { loginName } = payload;
  return request(`/api/system/users/${loginName}`, {
    method: 'PUT',
    headers: {
      'Content-Type': 'application/json',
    },
    mode: 'cors',
    body: JSON.stringify(payload),
  });
}

export async function deleteOne(payload) {
  const { loginName } = payload;
  return request(`/api/system/users/${loginName}`, {
    method: 'DELETE',
    headers: {
      'Content-Type': 'application/json',
    },
    mode: 'cors',
  });
}

export async function deleteList(payload) {
  return request('/api/system/users/', {
    method: 'DELETE',
    headers: {
      'Content-Type': 'application/json',
    },
    mode: 'cors',
    body: JSON.stringify(payload),
  });
}
