#system-web

Hi~这是基于ES6 + React + Dva + antd + system后台系统构建的后台管理前端系统。
* 克隆项目后使用npm或cnpm i 命令安装相关依赖。
* 开发过程中使用npm start命令，打开 http://localhost:8000。
* [API接口文档地址](http://apizza.cc/console/project/ea98511115baa3487a773c50c6084200/browse)

### 目录结构

```bash
├── /dist/           # 项目输出目录
├── /src/            # 项目源码目录
│ ├── /components/   # UI组件及UI相关方法
│ ├── /routes/       # 路由组件
│ │ └── App.js       # 路由入口
│ ├── /models/       # 数据模型
│ ├── /services/     # 数据接口
│ ├── /themes/       # 项目样式
│ ├── /utils/        # 工具函数
│ │ ├── config.js    # 项目常规配置
│ │ ├── menu.js      # 菜单及面包屑配置
│ │ ├── config.js    # 项目常规配置
│ │ ├── request.js   # 异步请求函数
│ │ └── theme.js     # 项目需要在js中使用到样式变量
│ ├── route.js       # 路由配置
│ ├── index.js       # 入口文件
│ └── index.html     
├── package.json     # 项目信息
├── .eslintrc        # Eslint配置
└── .roadhogrc.js    # roadhog配置
```
/.roadhogrc 文件中配置了服务的API接口地址,根据需要修改此配置。
```
"proxy": {
    "/api": {
      "target": "http://127.0.0.1:8080/",
      "changeOrigin": true,
      "pathRewrite": { "^/api" : "" }
    }
```
## 快速了解DVA,快速上手
[12 步 30 分钟，完成用户管理的 CURD 应用 (react+dva+antd)](https://github.com/sorrycc/blog/issues/18)
### 是否支持 IE8 ？不支持。
以下能帮你更好地理解和使用 dva ：

* 理解 dva 的 [8 个概念](https://github.com/dvajs/dva/blob/master/docs/Concepts_zh-CN.md) ，以及他们是如何串起来的
* 掌握 dva 的[所有 API](https://github.com/dvajs/dva/blob/master/docs/API_zh-CN.md)
* 查看 [dva 知识地图](https://github.com/dvajs/dva-knowledgemap) ，包含 ES6, React, dva 等所有基础知识
* 查看 [更多 FAQ](https://github.com/dvajs/dva/issues?q=is%3Aissue+is%3Aclosed+label%3Afaq)，看看别人通常会遇到什么问题
* 如果你基于 dva-cli 创建项目，最好了解他的 [配置方式](https://github.com/sorrycc/roadhog#配置)

###项目运行图片
![项目运行图片](https://git.oschina.net/uploads/images/2017/0708/195610_46930888_624302.gif "在这里输入图片标题")
![![项目运行图片](http://storage1.imgchr.com/knJa9.gif "在这里输入图片标题")
![![项目运行图片](http://storage1.imgchr.com/knGVJ.gif "在这里输入图片标题")
![![项目运行图片](http://storage1.imgchr.com/knY5R.gif "在这里输入图片标题")